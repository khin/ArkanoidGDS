extends StaticBody2D

export(int, 1, 5) var strength = 1
export(ColorArray) var colors

var sprite

signal destroyed

func _ready():
	if strength > colors.size():
		printerr("No color set for the required strength")
	
	sprite = get_node("Sprite")
	update_color()

func update_color():
	sprite.set_modulate(colors[strength - 1])

func damage():
	if strength == 1:
		queue_free()
		emit_signal("destroyed")
	else:
		strength -= 1
		update_color()
