extends Node

signal restart_game
signal exit_game

func _ready():
	get_node("button_restart").connect("pressed", self, "on_restart")
	get_node("button_exit").connect("pressed", self, "on_exit")

func on_restart():
	emit_signal("restart_game")

func on_exit():
	emit_signal("exit_game")
