extends Node

signal start_game
signal exit_game

func _ready():
	get_node("button_start").connect("pressed", self, "on_start")
	get_node("button_exit").connect("pressed", self, "on_exit")

func on_start():
	emit_signal("start_game")

func on_exit():
	emit_signal("exit_game")
