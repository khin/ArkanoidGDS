extends Node

signal game_finished(won)

var total_blocks
var blocks_count

func _ready():
	var blocks = get_node("blocks").get_children()
	
	total_blocks = blocks.size()
	blocks_count = 0
	
	for block in blocks:
		block.connect("destroyed", self, "block_destroyed")
	
	var ball = get_node("ball")
	ball.connect("dropped", self, "dropped_the_ball")

func block_destroyed():
	blocks_count += 1
	if blocks_count == total_blocks:
		print("player won!")
		emit_signal("game_finished", true)
	else:
		prints(total_blocks - blocks_count, "remaining")

func dropped_the_ball():
	print("player lost...")
	emit_signal("game_finished", false)
