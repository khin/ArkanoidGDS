extends KinematicBody2D

export(int, 0, 1000) var speed = 200
var height

func _ready():
	height = get_pos().height
	set_fixed_process(true)

func _fixed_process(delta):
	if Input.is_action_pressed("move_left"):
		move(Vector2(-speed * delta, 0))
	if Input.is_action_pressed("move_right"):
		move(Vector2(speed * delta, 0))
	
	set_pos(Vector2(get_pos().x, height))
