extends RigidBody2D

export(int, 0, 1000) var speed = 300
export(Vector2) var direction = Vector2(0, 1)

signal dropped

func _ready():
	direction = direction.normalized()
	connect("body_enter", self, "on_body_enter")
	
	apply_impulse(Vector2(), direction * speed)

func _integrate_forces(state):
	direction = state.get_linear_velocity().normalized()
	state.set_linear_velocity(direction * speed)

func on_body_enter(body):
	if body extends KinematicBody2D: # pad
		var dir = (get_pos() - body.get_pos()).normalized()
		set_linear_velocity(dir)
	elif body.has_method("damage"):
		body.damage()
	elif body.get_name() == "bottom":
		emit_signal("dropped")
