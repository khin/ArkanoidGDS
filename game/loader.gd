extends Node

var res_menu
var res_stage
var res_game_over
var res_win

var menu
var stage
var game_over
var win

func _ready():
	res_menu = preload("res://menu/main_menu.tscn")
	res_stage = load("res://stage/stage.tscn")
	res_game_over = load("res://menu/game_over.tscn")
	res_win = load("res://menu/win.tscn")

	if has_node("../menu"):
		menu = get_node("../menu")
		menu.connect("start_game", self, "on_start_game", [menu])
		menu.connect("exit_game", self, "on_exit_game")

func instance_menu():
	menu = res_menu.instance()
	menu.connect("start_game", self, "on_start_game", [menu])
	menu.connect("exit_game", self, "on_exit_game")
	
	get_tree().get_root().call_deferred("add_child", menu)

func instance_stage():
	stage = res_stage.instance()
	stage.connect("game_finished", self, "game_finished")
	
	get_tree().get_root().call_deferred("add_child", stage)

func instance_game_over():
	game_over = res_game_over.instance()
	game_over.connect("restart_game", self, "on_start_game", [game_over])
	game_over.connect("exit_game", self, "on_exit_game")
	
	get_tree().get_root().call_deferred("add_child", game_over)

func instance_win():
	win = res_win.instance()
	win.connect("restart_game", self, "on_start_game", [win])
	win.connect("exit_game", self, "on_exit_game")
	
	get_tree().get_root().call_deferred("add_child", win)

func on_start_game(scene):
	scene.queue_free()
	instance_stage()

func game_finished(success):
	stage.queue_free()
	if success:
		instance_win()
	else:
		instance_game_over()

func on_exit_game():
	get_tree().quit()
