extends Node

var sprite
var paused = false

func _ready():
	sprite = get_node("Sprite")
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("pause"):
		paused = not paused
		sprite.set_hidden(not paused)
		get_tree().set_pause(paused)
